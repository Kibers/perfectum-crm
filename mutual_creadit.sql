-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Гру 03 2018 р., 18:25
-- Версія сервера: 10.1.36-MariaDB
-- Версія PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `for_tests`
--

-- --------------------------------------------------------

--
-- Структура таблиці `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `the_amount_of_the_loan` decimal(7,2) NOT NULL DEFAULT '0.00',
  `creditor_id` int(11) NOT NULL DEFAULT '0',
  `debtor_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `loans`
--

INSERT INTO `loans` (`id`, `the_amount_of_the_loan`, `creditor_id`, `debtor_id`) VALUES
(1, '1000.00', 1, 2),
(2, '10000.00', 3, 7),
(3, '5000.00', 1, 6),
(4, '2000.00', 5, 4),
(5, '7000.00', 5, 2),
(6, '2500.00', 3, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `mutual_creadit`
--

CREATE TABLE `mutual_creadit` (
  `id` int(11) NOT NULL,
  `full_name` varchar(70) NOT NULL,
  `type_of_reationship` enum('кредитор','боржник') NOT NULL,
  `id_loan` varchar(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `mutual_creadit`
--

INSERT INTO `mutual_creadit` (`id`, `full_name`, `type_of_reationship`, `id_loan`) VALUES
(1, 'Малієв Олег Вікторович', 'кредитор', '1,3'),
(2, 'Шелест Микола Вікторович', 'боржник', '1'),
(3, 'Пастушенко Петро Павлович', 'кредитор', '2'),
(4, 'Багменко Ліда Василівна', 'боржник', '4'),
(5, 'Бойко Валентин Тарасович', 'кредитор', '4'),
(6, 'Мукоріз Люмила Володимірівна', 'боржник', '3'),
(7, 'Бажан Світлана Олегівна', 'боржник', '2');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `mutual_creadit`
--
ALTER TABLE `mutual_creadit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `mutual_creadit`
--
ALTER TABLE `mutual_creadit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
